#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>

#define FIFO_NAME "RESP_PIPE_69823"

void conectare(int* id)
{
    char conect[9];
    conect[0]=0x07;
    conect[1]=0x43;
    conect[2]=0x4f;
    conect[3]=0x4e;
    conect[4]=0x4e;
    conect[5]=0x45;
    conect[6]=0x43;
    conect[7]=0x54;
    conect[8]=0x0;
    unsigned int n=strlen(conect);
    //printf("%d\n",n);
    write(*id, &conect, n);
}

void ping_pong(int* id)
{
    char ping[6];
    ping[0]=0x04;
    ping[1]=0x50;
    ping[2]=0x49;
    ping[3]=0x4e;
    ping[4]=0x47;
    ping[5]=0x0;

    unsigned int n1=strlen(ping);
    write(*id, &ping,n1);

    char pong[6];
    pong[0]=0x04;
    pong[1]=0x50;
    pong[2]=0x4F;
    pong[3]=0x4e;
    pong[4]=0x47;
    pong[5]=0x0;

    unsigned int n2=strlen(pong);
    write(*id, &pong,n2);
    //printf("%s %s", ping, pong);

    unsigned int n3=69823;
    write(*id, &n3, sizeof(n3));

}

void create_shm(int* id, int size)
{
    int cheie=14121;
    char *sharedChar = NULL;
    int shmId=shmget(cheie, size, IPC_CREAT | 0664);
    if(shmId<0)
    {
        ///perror("aicii");
       char cr1[12];//create_shm //success,error
       cr1[0]=0x0A;
       cr1[1]=0x43;
       cr1[2]=0x52;
       cr1[3]=0x45;
       cr1[4]=0x41;
       cr1[5]=0x54;
       cr1[6]=0x45;
       cr1[7]=0x5f;
       cr1[8]=0x53;
       cr1[9]=0x48;
       cr1[10]=0x4d;
       cr1[11]=0x0;

       unsigned int n1=strlen(cr1);
       write(*id, &cr1, n1);

       char cr2[7];
       cr2[0]=0x05;
       cr2[1]=0x45;
       cr2[2]=0x52;
       cr2[3]=0x52;
       cr2[4]=0x4f;
       cr2[5]=0x52;
       cr2[6]=0x0;

       unsigned int n2=strlen(cr2);
       write(*id, &cr2, n2);
    }
    else
    {
        sharedChar = (char*)shmat(shmId, NULL, 0);
        if(sharedChar == (void*)-1)
        {

        char cr5[12];//create_shm //success,error
       cr5[0]=0x0A;
       cr5[1]=0x43;
       cr5[2]=0x52;
       cr5[3]=0x45;
       cr5[4]=0x41;
       cr5[5]=0x54;
       cr5[6]=0x45;
       cr5[7]=0x5f;
       cr5[8]=0x53;
       cr5[9]=0x48;
       cr5[10]=0x4d;
       cr5[11]=0x0;

       unsigned int n5=strlen(cr5);
       write(*id, &cr5, n5);

       char cr6[7];
       cr6[0]=0x05;
       cr6[1]=0x45;
       cr6[2]=0x52;
       cr6[3]=0x52;
       cr6[4]=0x4f;
       cr6[5]=0x52;
       cr6[6]=0x0;

       unsigned int n6=strlen(cr6);
       write(*id, &cr6, n6);

        }
        else 
        {
            //printf("aicii\n");
        char cr3[12];//create_shm //success,error
        cr3[0]=0x0A;
        cr3[1]=0x43;
        cr3[2]=0x52;
        cr3[3]=0x45;
        cr3[4]=0x41;
        cr3[5]=0x54;
        cr3[6]=0x45;
        cr3[7]=0x5f;
        cr3[8]=0x53;
        cr3[9]=0x48;
        cr3[10]=0x4d;
        cr3[11]=0x0;

        unsigned int n3=strlen(cr3);
        write(*id, &cr3, n3);

        char cr4[9];
        cr4[0]=0x07;
        cr4[1]=0x53;
        cr4[2]=0x55;
        cr4[3]=0x43;
        cr4[4]=0x43;
        cr4[5]=0x45;
        cr4[6]=0x53;
        cr4[7]=0x53;
        cr4[8]=0x0;
        //printf("%s %s\n", cr3,cr4);
        unsigned int n4=strlen(cr4);
        write(*id, &cr4, n4);

        }
    }

}

/*
void write_shm(int* id, int size, unsigned int ofset, int valoare)
{
    int cheie=14121;
    char *sharedChar = NULL;
    int shmId=shmget(cheie, size, IPC_CREAT | 0664);
    if(shmId<0)
    {
       
       char cr1[14];//write_to_shm //success,error
       cr1[0]=0x0C;
       cr1[1]=0x57;
       cr1[2]=0x52;
       cr1[3]=0x49;
       cr1[4]=0x54;
       cr1[5]=0x45;
       cr1[6]=0x5f;
       cr1[7]=0x54;
       cr1[8]=0x4f;
       cr1[9]=0x5f;
       cr1[10]=0x53;
       cr1[11]=0x48;
       cr1[12]=0x4d;
       cr1[13]=0x0;

       unsigned int n1=strlen(cr1);
       write(*id, &cr1, n1);

       char cr2[7];
       cr2[0]=0x05;
       cr2[1]=0x45;
       cr2[2]=0x52;
       cr2[3]=0x52;
       cr2[4]=0x4f;
       cr2[5]=0x52;
       cr2[6]=0x0;

       unsigned int n2=strlen(cr2);
       write(*id, &cr2, n2);
    }
    else
    {
        sharedChar = (char*)shmat(shmId, NULL, 0);
        if(sharedChar == (void*)-1)
        {

        char cr5[14];//write_to_shm //success,error
       cr5[0]=0x0C;
       cr5[1]=0x57;
       cr5[2]=0x52;
       cr5[3]=0x49;
       cr5[4]=0x54;
       cr5[5]=0x45;
       cr5[6]=0x5f;
       cr5[7]=0x54;
       cr5[8]=0x4f;
       cr5[9]=0x5f;
       cr5[10]=0x53;
       cr5[11]=0x48;
       cr5[12]=0x4d;
       cr5[13]=0x0;

       unsigned int n5=strlen(cr5);
       write(*id, &cr5, n5);

       char cr6[7];
       cr6[0]=0x05;
       cr6[1]=0x45;
       cr6[2]=0x52;
       cr6[3]=0x52;
       cr6[4]=0x4f;
       cr6[5]=0x52;
       cr6[6]=0x0;

       unsigned int n6=strlen(cr6);
       write(*id, &cr6, n6);

        }
        else 
        {
        
        if(ofset>0 && ofset<size)
            if(sharedChar[ofset]>0 && sharedChar[ofset]<size)
            {
                sharedChar[ofset]=valoare;

                char cr3[14];
                cr3[0]=0x0C;
                cr3[1]=0x57;
                cr3[2]=0x52;
                cr3[3]=0x49;
                cr3[4]=0x54;
                cr3[5]=0x45;
                cr3[6]=0x5f;
                cr3[7]=0x54;
                cr3[8]=0x4f;
                cr3[9]=0x5f;
                cr3[10]=0x53;
                cr3[11]=0x48;
                cr3[12]=0x4d;
                cr3[13]=0x0;

                unsigned int n3=strlen(cr3);
                write(*id, &cr3, n3);

                char cr4[9];
                cr4[0]=0x07;
                cr4[1]=0x53;
                cr4[2]=0x55;
                cr4[3]=0x43;
                cr4[4]=0x43;
                cr4[5]=0x45;
                cr4[6]=0x53;
                cr4[7]=0x53;
                cr4[8]=0x0;
                //printf("%s %s\n", cr3,cr4);
                unsigned int n4=strlen(cr4);
                write(*id, &cr4, n4);
            }
        }
    }   
}*/

void mapare_fisier(int* id,char* file_name)
{
    int fd=0;
    off_t size;
    char *data=NULL;

    fd = open(file_name, O_RDONLY); //map_file success/error
    if(fd == -1) {
       
        char f1[10];
        f1[0]=0x08;
        f1[1]=0x4d;
        f1[2]=0x41;
        f1[3]=0x50;
        f1[4]=0x5f;
        f1[5]=0x46;
        f1[6]=0x49;
        f1[7]=0x4c;
        f1[8]=0x45;
        f1[9]=0x0;
        unsigned int n1=strlen(f1);
        write(*id, &f1, n1);

        char f2[7];
        f2[0]=0x05;
        f2[1]=0x45;
        f2[2]=0x52;
        f2[3]=0x52;
        f2[4]=0x4f;
        f2[5]=0x52;
        f2[6]=0x0;

        unsigned int n2=strlen(f2);
        write(*id, &f2, n2);
    }

    size = lseek(fd, 0, SEEK_END);
    lseek(fd, 0, SEEK_SET);
    
    data = (char*)mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
    if(data == (void*)-1) {
        
        char f3[10];
        f3[0]=0x08;
        f3[1]=0x4d;
        f3[2]=0x41;
        f3[3]=0x50;
        f3[4]=0x5f;
        f3[5]=0x46;
        f3[6]=0x49;
        f3[7]=0x4c;
        f3[8]=0x45;
        f3[9]=0x0;
        unsigned int n3=strlen(f3);
        write(*id, &f3, n3);

        char f4[7];
        f4[0]=0x05;
        f4[1]=0x45;
        f4[2]=0x52;
        f4[3]=0x52;
        f4[4]=0x4f;
        f4[5]=0x52;
        f4[6]=0x0;

        unsigned int n4=strlen(f4);
        write(*id, &f4, n4);
        close(fd);
    }
    else 
    {
        char f5[10];
        f5[0]=0x08;
        f5[1]=0x4d;
        f5[2]=0x41;
        f5[3]=0x50;
        f5[4]=0x5f;
        f5[5]=0x46;
        f5[6]=0x49;
        f5[7]=0x4c;
        f5[8]=0x45;
        f5[9]=0x0;
        unsigned int n5=strlen(f5);
        write(*id, &f5, n5);

        char f6[9];
        f6[0]=0x07;
        f6[1]=0x53;
        f6[2]=0x55;
        f6[3]=0x43;
        f6[4]=0x43;
        f6[5]=0x45;
        f6[6]=0x53;
        f6[7]=0x53;
        f6[8]=0x0;
        unsigned int n6=strlen(f6);
        write(*id, &f6, n6);
    }

    munmap(data, size);
    close(fd);

}

int main(void)
{
    int fd = -1;
    int fd1 = -1;
    char tester[50];
    int lungime=0;

    if(mkfifo(FIFO_NAME, 0644) != 0) {
        perror("Could not create pipe");
        return 1;
    }

    fd1 = open("REQ_PIPE_69823", O_RDONLY);
    if(fd1 == -1) {
        perror("Could not open FIFO for reading");
        return 2;
    }
        
    

    fd = open(FIFO_NAME, O_WRONLY);
    if(fd == -1) {
        perror("Could not open FIFO for writing");
        return 3;
    }
    conectare(&fd);

    while(1)
    {
        read(fd1, &lungime, 1);
        read(fd1, &tester, lungime);
        //printf("%s", tester);
        //fflush(stdout);
        //printf("%d\n",lungime); 

        if(strncmp("PING\0",tester,4)==0) 
            {
            //printf("aici");
            ping_pong(&fd);
            break;
            }
        if(strncmp("CREATE_SHM\0",tester,10)==0) 
            {
            long nr=0;
            read(fd1, &nr, sizeof(nr));
            //printf("%ld\n",nr);
            create_shm(&fd,nr);
            break;
            }
        /*if(strncmp("WRITE_TO_SHM\0",tester,12)==0) 
            {
            unsigned int =0;
            int valoare=0;
            read(fd1, &nr, sizeof(nr));
            read(fd1, &valoare, sizeof(valoare));
            //printf("%ld\n",nr);
            write_shm(&fd,nr);
            break;
            }*/
        if(strncmp("MAP_FILE\0",tester,8)==0) 
            {
            char a[100];
            read(fd1, &a, 100);
            mapare_fisier(&fd,a);
            break;
            }
        break;
    }
    
    close(fd);
    close(fd1);
    unlink(FIFO_NAME);


    return 0;
}